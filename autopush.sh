#!/bin/sh

# Change to repository work directory
cd /Users/vladimir/Documents/GitHub/m-sa2-13-20

# Change to master branch
git checkout master

# Pushing changes
git push --all m-sa2-13-20-github
git push --all m-sa2-13-20-gitlab
git push --all m-sa2-13-20-bitbucket
